# build steps

## blockly-games

Please change the version number.

```
cd blockly-games
docker build -t blockly-games .
docker tag blockly-games shak12ml/blockly-games:1.1
docker login --username shak12ml   (password is shak12ml,123)
docker push shak12ml/blockly-games:1.1
```

## Source Code

Due to the size of the games, the source code is stored at
[Google Drive](https://drive.google.com/file/d/1bLggER-h7SLn-cvQ7Hml6qUU2CL7160n/view?usp=sharing).
