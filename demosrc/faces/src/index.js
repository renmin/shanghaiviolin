import React from 'react';
import ReactDOM from 'react-dom';
import 'ace-css/css/ace.min.css';

import App from './components/App';
import './index.css';

ReactDOM.render(<App/>, document.getElementById('root'));
