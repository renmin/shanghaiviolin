# build steps
## ai-duet

After change codes, run following commands to update the docker image.
```
cd ai-duet
docker build -t ai-duet .
docker tag ai-duet shak12ml/ai-duet:1.1
docker login --username shak12ml   (password is shak12ml,123)
docker push shak12ml/ai-duet:1.1
```

## blockly-games

Please check the "blockly-games/README.md". The source codes are too big that it is put to a Google Driver folder.

## faces

faces demo are purely browser-based. You can change the source codes, compile it and copy the compiled HTML/JS into "demo" folder.

## metacar

Please check the "demo/metacar/README.md" and "demosrc/metacar/README.md".
