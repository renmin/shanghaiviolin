import {BasicMotionEngine} from './basic_motion_engine';
import {ControlMotionEngine} from './control_motion_engine';
import {embeddedUrl, embeddedUrlI} from './embedded';
import {MetaCar} from './metacar';
import {MetaCarEditor} from './metacar_editor';

const metacar = {
  env: MetaCar,
  editor: MetaCarEditor,
  level: embeddedUrl,
  motion: {BasicMotion: BasicMotionEngine, ControlMotion: ControlMotionEngine}
}

export default metacar;