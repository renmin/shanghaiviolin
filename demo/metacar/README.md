Metacar is an open source project.(https://github.com/thibo73800/metacar)

This folder contains only static files of app demos using metacar. Metacar has
several games in `webapp/`, only `qtable.html` is revised.

We only use demo/webapp/qtable.html and make some UI changes, including:

*   webapp/qtable.html
*   webapp/public/general.css
*   webapp/public/js/q_table/*

The source files of metacar are in `demosrc/metacar`, you can rebuild
`metacar.min.js` and copy it to `demo/metacar/demo/dist`.
