#!/bin/bash

docker stop mnist

docker stop speech_command

docker stop image_classification

docker stop ai-duet

docker stop web

docker stop proxy

docker stop blockly-games
