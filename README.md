# Demo deployment
## Folder structure
"demo" is the static files served by a Nginx web server. It should be the compiled JS/HTML.

"demosrc" is the src files for "demo". If you want to modify HTML UI, please edit files in this folder.

"models" is the folder contains trained TensorFlow models. No need to touch it. It will be used by TensorFlow Serving server. Currently we only use the "mnist" model.

"proxy" folder contains the Nginx proxy configure. We have several docker instances. However we want to share one external domain name. So we setup proxy here.

"train" folder contains the TensorFlow source codes for the models in "models" folder. If you don't want to retrain the model, you can ignore this folder.

"start.sh" is the script to start the whole docker instances. This demo set doesn't use K8s. However you can move it to K8s.

"stop.sh" is the script to stop the whole docker instances.


## Deploy into single host with Docker

1. Copy current folder to a server via scp command.
As for how to copy folder to a server via scp, please check [scp doc](https://www.linux.com/learn/intro-to-linux/2017/2/how-securely-transfer-files-between-servers-scp).

Propose copy to /srv/k12ml. So you will have /srv/k12ml/demo, /srv/k12ml/models, /srv/k12ml/proxy, /srv/k12ml/stop.sh, /srv/k12ml/start.sh

2. SSH to the server and enter into the foler /srv/k12ml

3. stop the running containers via `stop.sh`
If there is no running containers in the server, it's also OK to run it `stop.sh` but just get few warning messages.

4. start the containers via `start.sh`

## Verify
- http://ip/index.html
Please replace ip with server's accessible IP.
It's entry page which lists the demos.
