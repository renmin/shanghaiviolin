#!/bin/bash

docker network create -d bridge mynet

docker run --rm \
    --net=mynet \
    -v "$(pwd)/models/:/models/" \
    -e MODEL_NAME=mnist \
    -d --name mnist \
    tensorflow/serving:1.13.0

docker run --rm \
    --net=mynet \
    -v "$(pwd)/models/:/models/" \
    -e MODEL_NAME=speech_command \
    -d --name speech_command \
    tensorflow/serving:1.13.0

docker run --rm \
    --net=mynet \
    -v "$(pwd)/models/:/models/" \
    -e MODEL_NAME=image_classification \
    -d --name image_classification \
    tensorflow/serving:1.13.0

docker run --rm --name=web \
    --volume=$(pwd)/demo/:/usr/share/nginx/html/ \
    --net=mynet \
    --detach \
    nginx:1.15.9

docker run --rm --name=ai-duet \
    --net=mynet \
    --detach \
    shak12ml/ai-duet:1.1

docker run --rm --name=proxy --publish=80:80 \
    --volume=$(pwd)/proxy/:/etc/nginx/conf.d/ \
    --net=mynet \
    --detach \
    nginx:1.15.9

docker run --rm --name=blockly-games \
    --net=mynet \
    --detach \
    shak12ml/blockly-games:1.1
