from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import app
from absl import flags
from absl import logging

import os
import tensorflow as tf
import numpy as np


def load_model(input_tensor,
               folder,
               frozen_file='mobilenet_v2_1.4_224_frozen.pb'):
  with open(os.path.join(folder, frozen_file), "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
  logits, = tf.import_graph_def(
      graph_def,
      input_map={"input:0": input_tensor},
      name="",
      return_elements=['MobilenetV2/Predictions/Reshape:0'])
  return logits


def log_to_tensorboard(session, log_dir):
  writer = tf.summary.FileWriter(log_dir)
  writer.add_graph(session.graph)
  logging.info("Visualize> tensorboard --logdir={}".format(log_dir))


def load_export(session, ckpt_dir, export_dir, log_dir):
  system_input = tf.placeholder(dtype=tf.float32,
                                shape=[None],
                                name='input')
  model_input = tf.reshape(system_input, [-1, 224, 224, 3])
  logits = load_model(model_input, ckpt_dir)
  argmax_output = tf.argmax(logits, axis=1)
  prob_output = tf.nn.softmax(logits)

  log_to_tensorboard(session, log_dir)
  tf.saved_model.simple_save(
      session,
      export_dir,
      inputs={"image": system_input},
      outputs={"classes": argmax_output, "probabilities": prob_output}
  )


def main(_):
  with tf.Session(graph=tf.Graph()) as session:
    load_export(session,
                flags.FLAGS.ckpt_dir,
                flags.FLAGS.export_dir,
                flags.FLAGS.tflog_dir)
  logging.info('All done')


if __name__ == '__main__':
  flags.DEFINE_string(
      name="ckpt_dir", default="/tmp/image_classification/ckpt-files",
      help="Directory for estimator graph and checkpoints")
  flags.DEFINE_string(
      name="export_dir", default="/tmp/image_classification/export",
      help="Directory for export saved model")
  flags.DEFINE_string(
      name="tflog_dir", default="/tmp/image_classification/log",
      help="Directory for TensorBoard log")

  app.run(main)
