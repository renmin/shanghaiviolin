checkpoints folder contains the trained weights with default flags.

Run following command to export SavedModel.

```
python export.py --start_checkpoint $(pwd)/checkpoints/conv.ckpt-18000 --saved_model_dir $(pwd)/savedmodel
```

The model doesn't support batching. Each audio sample has 16000 float values. Client can using moving-window to feed the audio stream into model and get the output. Maybe a CTC on top of it is more easier.
