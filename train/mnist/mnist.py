from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from absl import app
from absl import flags
from absl import logging

import tensorflow as tf
import numpy as np

def core_model():
  model = tf.keras.Sequential([
      tf.keras.layers.Conv2D(32, 3, activation=tf.nn.relu),
      tf.keras.layers.Conv2D(64, 3, activation=tf.nn.relu),
      tf.keras.layers.MaxPooling2D(2),
      tf.keras.layers.Conv2D(128, 3, activation=tf.nn.relu),
      tf.keras.layers.Conv2D(256, 3, activation=tf.nn.relu),
      tf.keras.layers.MaxPooling2D(2),
      tf.keras.layers.Flatten(),
      tf.keras.layers.Dense(128, activation=tf.nn.relu),
      tf.keras.layers.Dense(10)
  ])

  return model

def model_fn(features, labels, mode):
  """The model_fn argument for creating an Estimator."""
  model = core_model()
  image = features
  if isinstance(image, dict):
    image = features['image']

  # Model just for Predict
  if mode == tf.estimator.ModeKeys.PREDICT:
    logits = model(image, training=False)
    predictions = {
        'classes': tf.argmax(logits, axis=1),
        'probabilities': tf.nn.softmax(logits),
    }
    return tf.estimator.EstimatorSpec(
        mode=tf.estimator.ModeKeys.PREDICT,
        predictions=predictions,
        export_outputs={
            'classify': tf.estimator.export.PredictOutput(predictions)
        })

  # Model with Train Operators
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.AdamOptimizer()

    logits = model(image, training=True)
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
    accuracy = tf.metrics.accuracy(
        labels=labels, predictions=tf.argmax(logits, axis=1))

    # Name tensors to be logged with LoggingTensorHook.
    tf.identity(loss, 'cross_entropy')
    tf.identity(accuracy[1], name='train_accuracy')

    # Save accuracy scalar to Tensorboard output.
    tf.summary.scalar('train_accuracy', accuracy[1])

    return tf.estimator.EstimatorSpec(
        mode=tf.estimator.ModeKeys.TRAIN,
        loss=loss,
        train_op=optimizer.minimize(loss, tf.train.get_or_create_global_step()))

  # Model with Eval Operators
  if mode == tf.estimator.ModeKeys.EVAL:
    logits = model(image, training=False)
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
    return tf.estimator.EstimatorSpec(
        mode=tf.estimator.ModeKeys.EVAL,
        loss=loss,
        eval_metric_ops={
            'accuracy':
                tf.metrics.accuracy(
                    labels=labels, predictions=tf.argmax(logits, axis=1)),
        })

# Set up training and evaluation input functions.
def get_train_input_fn(x_train, y_train):
  def train_input_fn():
    """Prepare data for training."""
    dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    dataset = dataset.shuffle(buffer_size=1000)
    dataset = dataset.repeat(2)
    dataset = dataset.batch(64)
    iterator = dataset.make_one_shot_iterator()
    return iterator.get_next()
  return train_input_fn

def get_eval_input_fn(x_test, y_test):
  def eval_input_fn():
    """Prepare data for test."""
    dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
    dataset = dataset.batch(64)
    iterator = dataset.make_one_shot_iterator()
    return iterator.get_next()
  return eval_input_fn

def load_data():
  (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
  logging.info("Data loaded")
  logging.info("Shape:{}".format(x_train.shape))

  x_train = np.expand_dims(x_train, axis=-1).astype('float32')/255.0
  y_train = y_train.astype('int32')
  x_test = np.expand_dims(x_test, axis=-1).astype('float32')/255.0
  y_test = y_test.astype('int32')
  logging.info("Shape:{}".format(x_train.shape))
  return x_train, y_train, x_test, y_test

def get_estimator(model_dir):
  return tf.estimator.Estimator(model_fn=model_fn, model_dir=model_dir)

def serving_input_receiver_fn():
  system_input = tf.placeholder(dtype=tf.float32,
                                shape=[None],
                                name='input')
  # reshape (and other operators)
  model_input = tf.reshape(system_input, [-1, 28, 28, 1])

  # this is the dict that is then passed as "features" parameter to your model_fn
  features = {'image': model_input}
  receiver = {'image': system_input} # you specify multiple tensor as input
  return tf.estimator.export.ServingInputReceiver(features, receiver)

def train_eval_export(model_dir, export_dir):
  estimator = get_estimator(model_dir)

  x_train, y_train, x_test, y_test = load_data()
  estimator.train(
      input_fn=get_train_input_fn(x_train, y_train))
  eval_results = estimator.evaluate(
      input_fn=get_eval_input_fn(x_test, y_test))
  logging.info('Evaluation results: {}'.format(eval_results))

  export(estimator, export_dir)
  return estimator

def export(estimator, export_dir):
  exported_dir = estimator.export_saved_model(export_dir, serving_input_receiver_fn)
  logging.info('Saved model to: {}'.format(exported_dir))

def main(_):
  if flags.FLAGS.task == 'train_eval_export':
    train_eval_export(flags.FLAGS.model_dir, flags.FLAGS.export_dir)
  elif flags.FLAGS.task == 'export':
    estimator = get_estimator(flags.FLAGS.model_dir)
    export(estimator, flags.FLAGS.export_dir)
  else:
    raise Exception('not supported')

if __name__ == '__main__':
  flags.DEFINE_string(
      name="model_dir", default="/tmp/mnist/estimator",
      help="Directory for estimator graph and checkpoints")
  flags.DEFINE_string(
      name="export_dir", default="/tmp/mnist/savedmodel",
      help="Directory for export saved model")
  flags.DEFINE_enum(
      name="task", default="train_eval_export",
      enum_values=['train_eval_export', 'export'],
      help="Task type")

  app.run(main)
